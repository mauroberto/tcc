#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <iostream>
#include "bitmap.h"

#define MAX_VERTEX 20000

using namespace std;

int Vnbr,Enbr;
word bit[MAX_VERTEX][NWORDS(MAX_VERTEX)];
word pbit[MAX_VERTEX][NWORDS(MAX_VERTEX)];
int  pos[MAX_VERTEX];
int rec[MAX_VERTEX];
int record;
int set[MAX_VERTEX];
int level;
long long int subp;
clock_t clk;
int timeout;
double elapsed;
int s = 2;



int readFile(FILE * graphFile, int & n, int  & m) {
	char               type  = ' ';
	char               linestr[1024];
	char *             datastr;
	long               i, j;
    int                nedges;

	//printf("Reading graph...\n");

	while (type != 'p') {
		type = fgetc(graphFile);
		if (type != EOF) {

			/* header */
			if (type == 'c') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;
			}

			/* Vertices */
			if (type == 'p') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;

				datastr = strtok(linestr," ");

				datastr = strtok(NULL," ");
				n = atoi(datastr);

				datastr = strtok(NULL," ");
				m = atoi(datastr);

			}
		}
	}

	printf("Graph with %d vertices and %d edges density %f\n", n, m, (double)(2*m)/(n*(n-1)) );


  for(i=0;i<n;i++)     /* empty graph table */
    for(j=0;j<n/INT_SIZE+1;j++)
      bit[i][j] = 0;

  nedges = 0;
	type = fgetc(graphFile);
	while (type != EOF) {
		/* Edges */
		if (type == 'e') {
			datastr = fgets(linestr, 100, graphFile);


			if (datastr == NULL)
				return -1;

			datastr = strtok(linestr," ");
			i = atol(datastr) - 1;

			datastr = strtok(NULL," ");
			j = atol(datastr) - 1;

			//printf("edge %d %d\n", i , j);



      if( i != j ){
				nedges++;
				setbit(bit[i], j);
				setbit(bit[j], i);
      }

		}
		type = fgetc(graphFile);
	}

  int contador = 0;

  for(int i = 0; i < n; i++){
    for(int j = 0; j < n; j++){
      if( test(bit[i],j) ) contador++;
    }
  }

  contador = contador / 2;


  if( contador != m)
  {
    printf("Error bit adjacency graph representation\n");
    exit(1);
  }

	return 0;
}

void degree_order(){
	int degree[MAX_VERTEX];
	int used[MAX_VERTEX];
	int count;
	int delta = 0;

	/* order vertices */
  for(int i=0;i<Vnbr;i++) {
    degree[i] = 0;
    for(int j=0;j<Vnbr;j++)
      if ( test(bit[i],j) ) degree[i]++;
    if( degree[i] > delta) delta = degree[i];
  }

  for(int i=0;i<Vnbr;i++)
    used[i] = false;

  count = Vnbr - 1;
  do {
     int min_degree;
     int p;


     min_degree = delta + 1;
     p           = -1;

     for(int i=Vnbr-1;i>=0;i--){
       if((!used[i])&&(degree[i]<min_degree)){
         min_degree = degree[i];
         p = i;
       }
     }

     pos[count--] = p;
     used[p] = true;


     for(int j=0;j<Vnbr;j++)
       if((!used[j])&&(j!=p)&&(test(bit[p],j)))
         degree[j]--;

  } while(count >= 0);


}

bool isPlex(int * set, int level, int s){
    word C[NWORDS(Vnbr)];

    int W = high(Vnbr);

    for(int i=0; i<=W; i++) C[i] = 0LL;

    for(int i=0; i<level; i++) setbit(C, set[i]);

    //word R[W+1];
    word S[W+1];

		//printf("level %d\n", level );


    for(int i=0; i<level; i++){
        int v = set[i];

        for(int k = 0; k <= W; k++) S[k] = C[k] & bit[v][k];

				//printf("testa vertice %d(%d)\n", v, COUNT(S,W) );

        if( COUNT(S, W)  < level - s) return false;//  	for(int i = 0; i < Vnbr; i++){
//
//   		for(int k = 0; k < NWORDS(Vnbr); k++){
//      			pbit[i][k] = 0LL;
//    		}
//
//    		for(int j = 0; j < Vnbr; j++){
//      			if( test( bit[pos[i]], pos[j] ) ){
//        			setbit( pbit[i], j);
//      			}
//    		}
//  	}
    }

    return true;
}


void branching(word * U, int W, int limite){
    word R[W+1];
    word S[W+1];
    int UB = 0;
    int estavel[Vnbr];
    int count = 0;

    for(int i=0; i<=W; i++){
        R[i] = U[i];
    }

    while(1){
        if(UB == limite) return;

        int iv, jv;

        for(int k = 0; k <= W; k++){
            S[k] = R[k];
        }

        iv = 0;
        jv = LSB(S, iv, W);

        if(jv < 0) break;

        count = 0;

        while(jv >= 0){
            int v = indexbit(iv, jv);

            //if (count <= limite) resetbit(U, v);
            estavel[count++] = v;

            for(int k = 0; k <= W; k++){
                S[k] = S[k] & ~bit[v][k];
            }

            resetbit(S, v);

            jv = LSB(S, iv, W);
        }

        UB++;
        for(int k = 0; k < count; k++){
            int v = estavel[k];
            resetbit(U, v);
            resetbit(R, v);
        }
    }

    return ;
}

void geracao(word * U, int W, int level, word * newU){
    word R[W+1];
    for(int i=0; i<=W; i++) R[i] = U[i];
    for(int i=0; i<=W; i++) newU[i] = 0LL;

    int iv, jv;

    iv = 0;
    jv = LSB(R, iv, W);

    //printf("newU\n");
    while(jv >= 0){
        int v = indexbit(iv, jv);

        set[level+1] = v;
        if(isPlex(set, level+2, s)){
            //printf("vertex %d\n", v);
            setbit(newU, v);
        }

        resetbit(R, v);
        jv = LSB(R, iv, W);
    }
    //printf("]\n");
}

void basicPlex(word * U, int W, int level){
    int iv, jv;
    int iu, ju;
    //int sizeU = COUNT(U, W);
    word newU [W+1];
    word R [W+1];

    if(level > record){
			record = level;
			for(int i = 0; i < level; i++) rec[i] = set[i];
			printf("improving solution size %d\n", record );
    }
    subp++;

    for(int k = 0; k <= W; k++){
                R[k] = U[k] ;
            }


    branching(R, W, record- level);



    iv = W;
    jv = MSB(R, iv);



    iu = W;
    ju = MSB(U, iu);
    //printf("level %d size U %d\n", level, sizeU);

    while(jv >= 0){
        //if(sizeU + level <= record) return ;

        int v = indexbit(iv, jv);

				//printf("branching vertex %d\n", v);

        set[level] = v;

        //if( !isPlex(set, level+1, s) ) { printf("is not kplex\n"); exit(1); }

        resetbit(R, v);

        resetbit(U, v);


        jv = MSB(R, iv);

        ju = MSB(U, iu);


				//printf("generation newU\n");



        geracao(U, iu, level, newU);

        //printf("size newU %d\n", COUNT(newU, iv));
        basicPlex(newU, iu, level+1);
        //sizeU--;

    }
}

int main(int argc,char *argv[]){
	FILE *infile;

  	/* read input */
  	if(argc < 2) {
    		printf("Usage: BB [timeout] infile\n");
    		exit(1);
  	}

  	timeout = atoi( argv[1] );

  	printf("timeout %d s\n", timeout );

  	if((infile=fopen(argv[2],"r"))==NULL){
		printf("Error in graph file\n %s\n", argv[2]);
		exit(0);
  	}

  	/* read graph */
  	readFile(infile, Vnbr, Enbr);

  	/* "start clock" */
  	clk = clock();

  	//ordenaçao smallest-last
  	//degree_order();

  	//Reconstrução da matriz adjacência
    /*for(int i = 0; i < Vnbr; i++){
        for(int k = 0; k < NWORDS(Vnbr); k++){
            pbit[i][k] = 0LL;
        }

        for(int j = 0; j < Vnbr; j++){
            if( test( bit[pos[i]], pos[j] ) ){
                setbit( pbit[i], j);
            }
        }
    }*/

  	word U[ NWORDS(Vnbr) ];
  	for(int k = 0; k < NWORDS(Vnbr); k++){
  		U[k] = 0LL;
  	}
  	for(int i = 0; i < Vnbr; i++){
    		setbit(U, i);
  	}


  	subp  = 0LL;
  	printf("start search\n");
  	basicPlex(U, high(Vnbr), 0);

  	elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;

		if( isPlex(rec, record, s) )
		{
			printf("is 2-plex\n");
		}
		else
		{
			printf("is not 2-plex\n");
		}

  	printf("record          =  %10d\n", record );
  	printf("subp            =  %10lld\n", subp );
  	printf("time            =  %10.5f\n", elapsed );
  	//printf("cont1           =  %10lld\n", cont1 );
  	//printf("cont2           =  %10lld\n", cont2 );
  	//printf("cont3           =  %10lld\n", cont3 );
  	//printf("cont4           =  %10lld\n", cont4 );

	return 0;
}


