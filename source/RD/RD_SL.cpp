#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <vector>
#include "bitmap.h"

/*Constant*/
#define MAX_VERTEX 20000

using namespace std;

int Vnbr,Enbr;          /* number of vertices/edges */
int clique[MAX_VERTEX]; /* table for pruning */

word bit[MAX_VERTEX][MAX_VERTEX/INT_SIZE+1];

word pbit[MAX_VERTEX][NWORDS(MAX_VERTEX)];

int degree[MAX_VERTEX];
int pos[MAX_VERTEX];    /* reordering function */
int ord[MAX_VERTEX];    /*ordering by coloring*/
int set[MAX_VERTEX];    /* current clique */
int rec[MAX_VERTEX];	/* best clique so far */
int record;		/* weight of best clique */
int rec_level;          /* # of vertices in best clique */
int timeout;
int max_weight;
int cnt = 0;
long long int subp;
long long int oracle_calls;
int s = 2;

double elapsed;
clock_t clk;
word mask[INT_SIZE];


int fileerror();
void graph(FILE *fp);
void expand(word *R, int W, int level, int * nncnt);
bool isPlex(int * S, int satured_size, int v);
int readFile(FILE * graphFile, int & n, int  & m);
void geracao(word * U, int W, int level, word * newU, int * nncnt);



void printWord(word * newU, int W){
	printf("[ ");
	word X[NWORDS(Vnbr)];
	for(int i=0; i<=W; i++){
		X[i] = newU[i];
	}
	int a = 0, b = LSB(X, W, a);
	while(b >= 0){
		int v = indexbit(a, b);
		printf("%d, ", v);

		resetbit(X, v);
		b = LSB(X, W, a);
	}
	printf(" ]\n");
}


void make_saturated_list(int * set, int size, word * U, int W, int * nncnt, int * saturated_vertex, int & saturated_size){
	int u = set[size-1];
	saturated_size = 0;

	word R[W+1];

	for(int k=0; k <= W; k++){
		R[k] = U[k];
	}

	// Complexity O(|P'|)
	for(int j = 0; j < size-1; j++){
		int v = set[j];
		if( !test(bit[u],v) ){
			nncnt[v]++;
			#ifdef DEBUG
			printf("update nncnt[%d] = %d\n",v, nncnt[v]);
			#endif
		}
	}

	int iv = 0, jv = LSB(R, iv, W);

	while(jv >= 0){
		int v = indexbit(iv, jv);
		
		if(!test(bit[u], v)){
			nncnt[v]++;
		}

		resetbit(R, v);
		jv = LSB(R, iv, W);
	}

	for(int j = 0; j < size; j++){
		int v = set[j];
		if( nncnt[v] == s-1){
			saturated_vertex[saturated_size++] = v;
		}
	}
}

bool isPlex(int * S, int satured_size, int v){
	for(int i=0; i<satured_size; i++){
		int u = S[i];

		if(!test(bit[v], u)) return false;
	}

	return true;
}


void geracao(word * U, int W, int level, word * newU, int * nncnt){
	word R[W+1];
	for(int i=0; i<=W; i++){ 
		R[i] = U[i]; 
		newU[i] = 0LL;
	}

	int iv, jv;

	int S[Vnbr];
	int satured_size = 0;

	make_saturated_list(set, level, U, W, nncnt, S, satured_size);

	iv = 0;
	jv = LSB(R, iv, W);

	while(jv >= 0){
		int v = indexbit(iv, jv);

		if(nncnt[v] <= s-1 && isPlex(S, satured_size, v)){
			setbit(newU, v);
		}

		resetbit(R, v);
		jv = LSB(R, iv, W);
	}
}

int main (int argc,char *argv[]){
	int i,j,k,p;
	int wth;
	FILE *infile;


	/* read input */
	if(argc < 3) {
		printf("Usage: RDBB [timeout] infile\n");
		exit(1);
	}

	timeout = atoi( argv[1] );

	printf("timeout %d s\n", timeout );

	if((infile=fopen(argv[2],"r"))==NULL)
		fileerror();


	/* initialize mask */
	mask[0] = 1LL;
	for(i=1;i<INT_SIZE;i++)
		mask[i] = mask[i-1]<<1;

	/* read graph */
	readFile(infile, Vnbr, Enbr);


	/* "start clock" */
	clk = clock();

//
//	order();
//
//
//  for(int i = 0; i < Vnbr; i++)
//  {
//    pwt[i] = wt[ pos[i] ];
//
//    //printf("pwt[%d] = %d\n", i, pwt[i] );
//
//    for(int k = 0; k < NWORDS(Vnbr); k++)
//    {
//      pbit[i][k] = 0LL;
//    }
//
//    for(int j = 0; j < Vnbr; j++)
//    {
//      if( is_edge( pos[i], pos[j] ) )
//      {
//        setbit( pbit[i], j);
//      }
//    }
//  }
//
	word U[ NWORDS(Vnbr) ];
	word L[ NWORDS(Vnbr) ];


	for(int k = 0; k < NWORDS(Vnbr); k++){
		U[k] = 0LL;
		L[k] = 0LL;
	}

	record = 0;
	subp   = 0LL;
	oracle_calls = 0LL;

	int nncnt[Vnbr];

	for(int i = 0; i < Vnbr; i++){
		set[0] = i;

		int W = high(i);

		for(int j=0; j < Vnbr; j++){
			nncnt[j] = 0;
		}

		geracao(U, W, 1, L, nncnt);
		setbit(U, i);
		expand(L, W, 1, nncnt);



		elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;

		printf("%d/%d (max %d) %.5f s\n", i+1, Vnbr, record, elapsed );

		clique[i] = record;
	}

	elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;

	printf("best clique: ");
	for(int i = 0; i < rec_level; i++){
		printf("%d ", rec[i]+1);
	}

	printf("\n");
	printf("subp                 = %10lld\n", subp);
	printf("oracle_calls         = %10lld\n", oracle_calls);
	printf("record               = %10d\n", record);
	printf("rec_level            = %10d\n", rec_level);
	printf("time                 = %10.5f\n", elapsed);

}



void coloring(word * U, int W, int limite){
	word R[W+1];
	word S[W+1];
	int UB = 0;
	int estavel[Vnbr];
	int count = 0;

	for(int i=0; i<=W; i++){
		R[i] = U[i];
	}

	while(1){
		if(UB == limite) return;

		int iv, jv;

		for(int k = 0; k <= W; k++){
			S[k] = R[k];
		}

		iv = 0;
		jv = LSB(S, iv, W);

		if(jv < 0) break;

		count = 0;

		while(jv >= 0){
			int v = indexbit(iv, jv);

			estavel[count++] = v;

			for(int k = 0; k <= W; k++){
				S[k] = S[k] & ~bit[v][k];
			}

			resetbit(S, v);

			jv = LSB(S, iv, W);
		}

		UB++;
		for(int k = 0; k < count; k++){
			int v = estavel[k];
			resetbit(U, v);
			resetbit(R, v);
		}
	}
}

void dolls(word * R, int W, int limit){
	int iv, jv;

	iv = 0;
	jv = LSB(R, iv, W);

	if(limit == 0) return;

	while( jv >= 0){
		int v = indexbit(iv,jv);

		if(clique[v] > limit) break;

		resetbit(R, v);

		jv = LSB(R, iv, W);
	}
}

void expand(word *U, int W, int level, int * nncnt){
	word R[W+1];
	//word S[W+1];

	word newU[W+1];
	int iv,jv,v;
	int iu,ju,u;


	subp++;

	if(++cnt > 1000) {
		cnt = 0;
		elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
		if(elapsed >= timeout) {
			printf("timeout\n");
			printf("best clique : ");
			for(int i = 0; i < rec_level; i++)
				printf("%d " , rec[i]+1);
			printf("\n");
			printf("subp                 = %10lld\n", subp);
			printf("record               = %10d\n", record);
			printf("rec_level            = %10d\n", rec_level);
			printf("time                 = %10.5f\n", elapsed);
			exit(0);
		}

	}

	if(level > record) {
		record = level;
		rec_level = level;
		for (int i=0;i<level;i++) rec[i] = set[i];
	}


	for(int k = 0; k <= W; k++){
		R[k] = U[k];
	//	S[k] = U[k];
	}

	dolls(R, W, record - level);

	//if( COUNT(S, W) == 0 ) return ;

	//coloring(R, W, record-level);


	//if( COUNT(S, W) <= COUNT(R, W) ){
	//	for(int k = 0; k <= W; k++){
	//		R[k] = S[k];
	//	}
	//}


	iv = W;
	jv = MSB(R, iv);


	iu = W;
	ju = MSB(U, iu);


	//printf("%d\n", level);
	while(jv >= 0){
		int newNncnt[Vnbr];
		for(int k = 0; k < Vnbr; k++){
			newNncnt[k] = nncnt[k];
		}

		v = indexbit(iv,jv);

		set[level] = v;


		resetbit(R, v);
		resetbit(U, v);

		jv = MSB(R, iv);
		ju = MSB(U, iu);

		geracao(U, iu, level+1, newU, newNncnt);
		
		expand(newU, iu, level+1, newNncnt);
	}
}

int readFile(FILE * graphFile, int & n, int  & m) {
	char type  = ' ';
	char linestr[1024];
	char * datastr;
	long i, j;
	int nedges;

	while (type != 'p') {
		type = fgetc(graphFile);
		if (type != EOF) {
			/* header */
			if (type == 'c') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;
			}

			/* Vertices */
			if (type == 'p') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;

				datastr = strtok(linestr," ");

				datastr = strtok(NULL," ");
				n = atoi(datastr);

				datastr = strtok(NULL," ");
				m = atoi(datastr);

			}
		}
	}

	printf("Graph with %d vertices and %d edges density %f\n", n, m, (double)(2*m)/(n*(n-1)) );


  	for(i=0; i<n; i++) degree[i] = 0;
  	for(i=0;i<n;i++)
		for(j=0;j<n/INT_SIZE+1;j++)
  			bit[i][j] = 0;

  	nedges = 0;
	type = fgetc(graphFile);
	while (type != EOF) {
		/* Edges */
		if (type == 'e') {
			datastr = fgets(linestr, 100, graphFile);


			if (datastr == NULL)
				return -1;

			datastr = strtok(linestr," ");
			i = atol(datastr) - 1;

			datastr = strtok(NULL," ");
			j = atol(datastr) - 1;

			//printf("edge %d %d\n", i , j);



  			if( i != j ){
				nedges++;
				setbit(bit[i], j);
				setbit(bit[j], i);
				degree[i]++;
				degree[j]++;
  			}

		}
		type = fgetc(graphFile);
	}

	int contador = 0;

	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			if( test(bit[i],j) ) contador++;
		}
	}

	contador = contador / 2;


	if( contador != m ){
		printf("Error bit adjacency graph representation\n");
		exit(1);
	}

	return 0;
}


int fileerror(){
	printf("Error in graph file\n");
	exit(0);
}
