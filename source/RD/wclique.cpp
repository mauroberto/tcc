#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
//#include <sys/resource.h>
#define INT_SIZE (8*sizeof(int))
#define TRUE 1
#define FALSE 0
#define MAX_VERTEX 20000 /* maximum number of vertices */
#define is_edge(a,b) (bit[a][b/INT_SIZE]&(mask[b%INT_SIZE]))

int Vnbr,Enbr;          /* number of vertices/edges */
int clique[MAX_VERTEX]; /* table for pruning */
int bit[MAX_VERTEX][MAX_VERTEX/INT_SIZE+1];
int wt[MAX_VERTEX];

int pos[MAX_VERTEX];    /* reordering function */
int set[MAX_VERTEX];    /* current clique */
int rec[MAX_VERTEX];	/* best clique so far */
int record;		/* weight of best clique */
int rec_level;          /* # of vertices in best clique */
long long int subp;
int max_weight;
clock_t clk;
double elapsed;
int timeout;
int cnt = 0;

int s = 2;

unsigned mask[INT_SIZE];
void graph();           /* reads graph */
int fileerror();
void graph(FILE *fp);
int sub(int ct,int *table,int level);
int readFile(FILE * graphFile, int & n, int  & m);


void make_satured_list(int * set, int level, int s, int * S, int & satured_size){
	int u = set[level];
	int nncnt[MAX_VERTEX];	
	for(int i=0; i<Vnbr; i++){
		nncnt[i] = 0;
	}
	
	satured_size = 0;

	for(int i=0; i < level; i++){
		int v = set[i];
		
		if(!is_edge(u, v)){
			nncnt[v]++;
			nncnt[u]++;

			if(nncnt[v] == s-1){
				S[satured_size] = v;
				satured_size++;
			}
		}
	}

	if(nncnt[u] == s-1){
		S[satured_size] = u;
		satured_size++;
	}

	for(int i=0; i<Vnbr; i++){
		if(nncnt[i] != 0)
			printf("nncnt[%d] = %d\n", i, nncnt[i]);
	}
}

bool isPlex(int * S, int satured_size, int v){
	for(int i=0; i<satured_size; i++){
		int u = S[i];

		if(!is_edge(u, v)) return false;
	}

	return true;
}

bool isPlex2(int * C, int size){
	int degree[Vnbr];

	for(int i=0; i<Vnbr; i++) degree[i] = 0;

	for(int i = 0; i<size; i++){
		int v = C[i];
		for (int j=0; j<size; j++){
			int u = C[j];
			if(is_edge(u,v)){
				degree[u]++;
				degree[v]++;
			}
		}
	}	

	for(int i=0; i<Vnbr; i++){
		if(degree[C[i]] != 0)
			printf("degree[%d] = %d\n", C[i], degree[C[i]]);
	}

	for(int i=0; i<size; i++){
		if(degree[C[i]] < size-s){
			return false;
		}
	}

	return true;
}

main (int argc,char *argv[])
{
  int i,j,k;
  int wth;
	int sort_option;
  FILE *infile;


  /* read input */
  if(argc < 3) {
    printf("Usage: wclique timeout infile\n");
    exit(1);
  }

  timeout = atoi( argv[1] );

  printf("timeout %d s\n", timeout );

  if((infile=fopen(argv[2],"r"))==NULL)
    fileerror();

  /* initialize mask */
  mask[0] = 1;
  for(i=1;i<INT_SIZE;i++)
    mask[i] = mask[i-1]<<1;

  /* read graph */
  readFile(infile, Vnbr, Enbr);

  /* "start clock" */
  clk = clock();

	//order();

	for(int i=0; i<Vnbr; i++){
		pos[i] = i;
	}


  /* main routine */
  record = 0;
  subp = 0LL;
  for(i=0;i<Vnbr;i++) {
     sub(i,pos,0);
     clique[pos[i]] = record;
     elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
     printf("%d/%d (max %d) %.5f s\n", i+1, Vnbr, record, elapsed );
  }
  elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;

  printf("best clique: ");
  for(int i = 0; i < record; i++)
  {
    printf("%d ", rec[i]);
  }
  printf("\n");
  printf("subp                 = %10lld\n", subp);
  printf("record               = %10d\n", record);
  printf("time                 = %10.5f\n", elapsed);


}

int sub(int ct,int *table,int level)
{
  register int i,j,k;
  int best;
  int curr_level;
  int newtable[Vnbr];
  int new_size;
  int *p1,*p2;
  int S[Vnbr];
  int satured_size;

  subp++;

   if(++cnt > 1000) {
			cnt = 0;
			elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
			if(elapsed >= timeout) {
          printf("timeout\n");
          printf("record               = %10d\n", record);
          printf("rec_level            = %10d\n", rec_level);
          printf("best clique: ");
          for(int i = 0; i < record; i++)
          {
            printf("%d ", rec[i]);
          }
          printf("\n");
          exit(0);
       }
  }


  if(ct<=0) { /* 0 or 1 elements left; include these */
    if(ct==0) {
      set[level++] = table[0];
    }
    if(level>record) {
      record = level;
      for (i=0;i<level;i++) {
        rec[i] = set[i];
      }
      printf("new lb clique size %d\n", record);
    }
    return 0;
  }

  for(i=ct;i>=0;i--) {
    if((level==0)&&(i<ct)) return 0;
    k = table[i];
    if((level>0)&&(clique[k]<=(record-level))) return 0;  /* prune */
    set[level] = k;
    if(i<=(record-level)) return 0; /*prune */
    

    printf("generate candidate\n");
	make_satured_list(set, level, s, S, satured_size);
	new_size = 0;
	for(int j = 0; j < i ; j++){
		int v = pos[j];
		set[level+1] = v;
		if(isPlex(S, satured_size, v)){
			newtable[new_size++] = v;
		}
		if(isPlex(S, satured_size, v) != isPlex2(set, level+2)){
		
			printf("Pos %d = %d \n", j, v);
			printf("IsPlex %d\n", isPlex(S, satured_size, v));
			printf("IsPlex2 %d\n", isPlex2(set, level+2));
			printf("Error\n");
			exit(0);
		}
	}

	printf("curr_size %d new_size %d\n", i, new_size);
    //int pausa;
    //scanf("%d", &pausa);

    curr_level = level + 1;
    if(new_size+curr_level <= record) continue;
    sub(new_size-1,newtable,curr_level);
  }
  return 0;
}


int readFile(FILE * graphFile, int & n, int  & m) {
	char               type  = ' ';
	char               linestr[1024];
	char *             datastr;
	int                r;
	long               i, j;
  int                nedges;

	//printf("Reading graph...\n");

	while (type != 'p') {
		type = fgetc(graphFile);
		if (type != EOF) {

			/* header */
			if (type == 'c') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;
			}

			/* Vertices */
			if (type == 'p') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;

				datastr = strtok(linestr," ");

				datastr = strtok(NULL," ");
				n = atoi(datastr);

				datastr = strtok(NULL," ");
				m = atoi(datastr);

			}
		}
	}

	printf("Graph with %d vertices and %d edges.\n", n, m);
	printf("density %lf\n", (double)(2*m)/(n*(n-1)));

  for(i=0;i<n;i++)     /* empty graph table */
    for(j=0;j<n/INT_SIZE+1;j++)
      bit[i][j] = 0;

  nedges = 0;
	type = fgetc(graphFile);
	while (type != 'n' && type != EOF) {
		/* Edges */
		if (type == 'e') {
			datastr = fgets(linestr, 100, graphFile);


			if (datastr == NULL)
				return -1;

			datastr = strtok(linestr," ");
			i = atol(datastr) - 1;

			datastr = strtok(NULL," ");
			j = atol(datastr) - 1;


      nedges++;
			if( i != j )
			{
				bit[i][j/INT_SIZE] |= mask[j%INT_SIZE];
				bit[j][i/INT_SIZE] |= mask[i%INT_SIZE];
			}
		}
		type = fgetc(graphFile);
	}

  max_weight = 0;

	while (type != EOF) {
		/* Edges */
		if (type == 'n') {
			datastr = fgets(linestr, 100, graphFile);
			if (datastr == NULL)
				return -1;

			datastr = strtok(linestr," ");
			i = atol(datastr) - 1;

			datastr = strtok(NULL," ");
			j = atol(datastr);

			wt[i] = j;

			if( j > max_weight)
        max_weight = j;
		}
		type = fgetc(graphFile);
	}

	if( max_weight == 0)
	{
    for(int i = 0; i < Vnbr; i++)
    {
      wt[i] = ((i+1)%200)+1;
      if( wt[i] > max_weight) max_weight = wt[i];
    }
	}

	return 0;
}



int fileerror()
{
  printf("Error in graph file\n");
  exit(1);
}
