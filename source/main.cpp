#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include <time.h>
//#include <ctime>
#include <ilcplex/ilocplex.h>

#define NUM_ENTRADAS1 23
#define NUM_ENTRADAS2 27
//23 e 27

#define TEMPO_MAXIMO 7200

using namespace std;

#include "mtz.cpp"
#include "novo2.cpp"

void criarArquivoSolucao(IloCplex &cplex, IloNumVarArray *y, int **grafo, int **conv, int n, int raiz) {
    FILE *sol;
    IloNum eps = cplex.getParam(IloCplex::EpInt);
    char solucao[200] = "Solucao.txt";
    if((sol = fopen(solucao, "w")) == NULL){
        printf("Arquivo não pode ser aberto\n");
        exit(1);
    }
    fprintf(sol, "%d", n);
    int i, j;
    for (i = 0; i < n; i++){
		for (j = 0; j < n; j++){
            if (j != raiz && grafo[i][j] != -1){
                double valor = cplex.getValue(y[i][conv[i][j]]);
                if(valor >= (1 - eps)) fprintf(sol, "\n%d %d", i+1, j+1);
            }
		}
	}
    fclose(sol);
}
string entradas1[NUM_ENTRADAS1] = {
                                "z50-200-199.gcc", "z50-200-398.gcc", "z50-200-597.gcc",
                                "z50-200-995.gcc", "z100-300-448.gcc", "z100-300-897.gcc",
                                "z100-300-1344.gcc", "z100-500-1247.gcc", "z100-500-2495.gcc",
                                "z100-500-3741.gcc", "z100-500-6237.gcc", "z100-500-12474.gcc",
                                "z200-600-1797.gcc", "z200-600-3594.gcc", "z200-600-5391.gcc",
                                "z200-800-3196.gcc", "z200-800-6392.gcc", "z200-800-9588.gcc",
                                "z200-800-15980.gcc", "z300-800-3196.gcc", "z300-1000-4995.gcc",
                                "z300-1000-9990.gcc", "z300-1000-14985.gcc"};

string entradas2[NUM_ENTRADAS2] = {
                                "z50-200-type2-3903.gcc", "z50-200-type2-4877.gcc", "z50-200-type2-5864.gcc",
                                "z100-300-type2-8609.gcc", "z100-300-type2-10686.gcc", "z100-300-type2-12761.gcc",
                                "z100-500-type2-24740.gcc", "z100-500-type2-30886.gcc", "z100-500-type2-36827.gcc",
                                "z200-400-13660.gcc", "z200-400-17089.gcc", "z200-400-20469.gcc",
                                "z200-600-34504.gcc", "z200-600-42860.gcc", "z200-600-50984.gcc",
                                "z200-800-62625.gcc", "z200-800-78387.gcc", "z200-800-93978.gcc",
                                "z300-600-31000.gcc", "z300-600-38216.gcc", "z300-600-45310.gcc",
                                "z300-800-59600.gcc", "z300-800-74500.gcc", "z300-800-89300.gcc",
                                "z300-1000-96590.gcc", "z300-1000-120500.gcc", "z300-1000-144090.gcc"};
void mainAll(int versaoR){
	int i;
    string path1 = "samer-urrutia-2015-instances//type1//";
    string path2 = "samer-urrutia-2015-instances//type2//";

	for(i = 0; i < NUM_ENTRADAS1; i++){
        string arq_nome = path1 + entradas1[i];
		solverV1(arq_nome, versaoR);
	}

	for(i = 0; i < NUM_ENTRADAS2; i++){
        string arq_nome = path2 + entradas2[i];
		solverV1(arq_nome, versaoR);
	}

	for(i = 0; i < NUM_ENTRADAS1; i++){
        string arq_nome = path1 + entradas1[i];
		solverV2(arq_nome, versaoR);
	}

	for(i = 0; i < NUM_ENTRADAS2; i++){
        string arq_nome = path2 + entradas2[i];
		solverV2(arq_nome, versaoR);
	}
}

int main(int argc, char *agrv[]){
    mainAll(1);
    mainAll(3);
    return 0;
}
