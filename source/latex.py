import pickle

instancias = [
("brock200_1",200,0.745,21,-1,0.327,0.312),
("brock200_2",200,0.496,12,-1,0.001,0.001),
("brock200_3",200,0.605,15,-1,0.001,0.001),
("brock200_4",200,0.658,17,-1,0.062,0.063),
("brock400_1",400,0.75,27,693,341,348),
("brock400_2",400,0.75,29,297,144,140),
("brock400_3",400,0.75,31,468,229,240),
("brock400_4",400,0.75,33,248,133.4,143),
("brock800_1",800,0.65,23,9347),
("brock800_2",800,0.65,24,8368),
("brock800_3",800,0.65,25,5755),
("brock800_4",800,0.65,26,3997),
("c-fat200-1",200,0.077,12,-1),
("c-fat200-2",200,0.163,24,-1),
("c-fat200-5",200,0.426,58,-1),
("c-fat500-1",500,0.036,14,-1),
("c-fat500-2",500,0.073,26,-1),
("c-fat500-5",500,0.186,64,-1),
("c-fat500-10",500,0.374,126,-1),
("hamming6-2",64,0.905,32,-1),
("hamming6-4",64,0.349,4,-1),
("hamming8-2",256,0.969,128,-1),
("hamming8-4",256,0.639,16,-1,0.032,0.015),
("hamming10-2",1024,0.990,512,-1,0.031,0.063),
("johnson8-2-4",28,0.556,4,-1),
("johnson8-4-4",70,0.768,14,-1),
("johnson16-2-4",120,0.765,8,-1,0.078,0.062),
("keller4",171,0.649,11,-1,0.016,0.001),
("MANN_a9",45,0.927,16,-1),
("MANN_a27",378,0.990,126,0.8,0.312,0.187),
("MANN_a45",1035,0.996,345,281,144,42.40),
("p_hat300-1",300,0.244,8,-1,),
("p_hat300-2",300,0.489,25,-1,0.015,0.001),
("p_hat300-3",300,0.744,36,2.5,1.25,1.31),
("p_hat500-1",500,0.253,9,-1,0.001,0.001),
("p_hat500-2",500,0.505,36,0.7,0.39,0.39),
("p_hat500-3",500,0.752,50,150,73.90,76.10),
("p_hat700-1",700,0.249,11,-1,0.047,0.047),
("p_hat700-2",700,0.498,44,5.6,3.51,3.79),
("p_hat700-3",700,0.748,62,2392,1720,1640),
("p_hat1000-1",1000,0.245,10,-1,0.328,0.421),
("p_hat1000-2",1000,0.490,46,221,187,193),
("p_hat1500-1",1500,0.253,12,-1,3.23,3.92),
("p_hat1500-2",1500,0.506,65,16512),
("san200_0.7_1",200,0.700,30,-1),
("san200_0.7_2",200,0.700,18,-1),
("san200_0.9_1",200,0.90,70,0.22,0.156,0.094),
("san200_0.9_2",200,0.90,60,0.4,0.109,0.062),
("san200_0.9_3",200,0.90,44,-1,0.015,0.015),
("san400_0.5_1",400,0.500,13,-1,0.016,0.016),
("san400_0.7_1",400,0.700,40,0.54,0.234,0.125),
("san400_0.7_2",400,0.700,30,0.13,0.078,0.063),
("san400_0.7_3",400,0.700,22,1.4,0.514,0.437),
("san400_0.9_1",400,0.900,100,0.1,0.141,0.031),
("san1000",1000,0.502,15,2.1,0.68,0.375),
("sanr200_0.7",200,0.702,18,0.34,0.125,0.125),
("sanr200_0.9",200,0.898,42,41,18.20,13.90),
("sanr400_0.5",400,0.501,13,-1,0.297,0.327),
("sanr400_0.7",400,0.700,21,181,91,102.0),
("DSJC500_5",500,0.502,13,-1),
("DSJC1000_5",1000,0.500,15,-1),
("gen200_p0.9_44",200,0.900,44,0.47,0.328,0.187),
("gen200_p0.9_55",200,0.900,55,1.2,0.546,0.437),
("gen400_p0.9_55",400,0.900,55,58431),
("gen400_p0.9_65",400,0.900,65,151597),
("gen400_p0.9_75",400,0.900,65,294175),
("C125.9",125,0.900,34,-1,0.031,0.016),
("C250.9",250,0.899,44,3257),
]



with open('projects.tex', 'w') as texfile:
	texfile.write('\\documentclass[a4paper,twoside]{report}\n')
	texfile.write('\\usepackage{longtable}\n')
	texfile.write('\\usepackage{graphicx}\n')	
	texfile.write('\\begin{document}\n')
	
	texfile.write('\\begin{center}\n')	
			
	texfile.write('\\begin{table}[H]\n')
	texfile.write('\\scalebox{0.6}{\n')	
	texfile.write('\\begin{tabular}{cccccccccc}\n')
	texfile.write('\\hline\n')
	texfile.write('Grafo &  (n,d) & W & Ostergaard & Yamagucchi & Shimizu\\\\ \n')
	
	texfile.write('\\hline\n')	
	
	for inst in instancias:
		
		inst_name = inst[0].replace("_","\_")
		
		texfile.write(' %s ' % ( inst_name ) )
		
		texfile.write(' & (%d,%.3f)' % (inst[1], inst[2]))
		
		flag1 = True
		
		menor = 3600
		w = -1
		
		try:
			filename = "../ostergaard2001/resultado/%s.out" % (inst[0])
			f = open(filename)
			lines = f.readlines();				
			r1   = int(lines[0].split("=")[1])
			t1   = float(lines[2].split("=")[1])
			sub1 = int(lines[3].split("=")[1])	
			w = r1
		except:
			flag1 = False
		
		
		if flag1:
			if t1 < menor:
				menor  = t1
		
		flag2 = True
		
		try:
			filename = "../yamaguchi/resultado/%s.out" % (inst[0])
			f = open(filename)
			lines = f.readlines();				
			r2   = int(lines[0].split("=")[1])
			t2   = float(lines[2].split("=")[1])
			sub2 = int(lines[3].split("=")[1])	
			if w != -1:
				if r2 != w:
					print "ERRO\n"
			w = r2
		except:
			flag2 = False
		
		if flag2:
			if t2 < menor:
				menor  = t2
		
		if flag1 and flag2:
			if r1 != r2:
				print "ERRO"
		
		
		
		flag3 = True
		
		try:
			filename = "../optcliquer/resultado/%s.out" % (inst[0])
			f = open(filename)
			lines = f.readlines();				
			r3   = int(lines[0].split("=")[1])
			t3   = float(lines[2].split("=")[1])
			sub3 = int(lines[3].split("=")[1])	
			if w != -1:
				if r3 != w:
					print "ERRO\n"
			w = r3
		except:
			flag3 = False
		
		if flag3:
			if t3 < menor:
				menor  = t3
		
		razao1 = menor
		
		#flag4 = True
		
		
		#try:
		#	filename = "../DBB/resultado/%s.out" % (inst[0])
		#	f = open(filename)
		#	lines = f.readlines();				
		#	r4   = int(lines[0].split("=")[1])
		#	t4   = float(lines[2].split("=")[1])
		#	sub4 = int(lines[3].split("=")[1])	
		#	if w != -1:
		#		if r4 != w:
		#			print "ERRO\n"
		#	
		#	w = r4
		#except:
		#	flag4 = False
		
		#if flag4:
		#	if t4 < menor:
		#		menor  = t4
		
		
		#if flag4 and flag2:
		#	if r4 != r2:
		#		print "ERRO\n"
		
		#razao2 = t4
		
		#if flag3 and flag4:
		#	if t3 < t4:
		#		razao2 = t3
		#	else:
		#		razao2 = t4
			
		
		texfile.write('& %d ' % ( w ) )
		
		if flag1:
			if t1 <= menor:
				texfile.write('& \\textbf{%.5f} ' % ( t1 ) )
			else:
				texfile.write('& %.5f ' % ( t1 ) )
		else:
			texfile.write('& fail '  )
		
		if flag2:
			if t2 <= menor:
				texfile.write('& \\textbf{%.5f} ' % ( t2 ) )
			else:
				texfile.write('& %.5f ' % ( t2 ) )
		else:
			texfile.write('& fail '  )
		
		if flag3:
			if t3 <= menor:
				texfile.write('& \\textbf{%.5f} ' % ( t3 ) )
			else:
				texfile.write('& %.5f ' % ( t3 ) )
		else:
			texfile.write('& fail '  )
		
		
		#if flag4:
		#	
		#	texfile.write('& ')
		#	k = razao1/t4			
		#	if k > 4.0 and k < 16:
		#		texfile.write('$\\bigstar$')
		#	elif k >= 16 and k < 64:
		#		texfile.write('$\\bigstar\\bigstar$')
		#	elif k >= 64 and k < 256:
		#		texfile.write('$\\bigstar\\bigstar\\bigstar$')
		#	elif k >= 256 and k < 1024:
		#	    texfile.write('$\\bigstar\\bigstar\\bigstar\\bigstar$')
		#	elif k >= 1024:
		#	    texfile.write('$\\bigstar\\bigstar\\bigstar\\bigstar\\bigstar$')
		#	
			
		#	if t4 <= razao1:
		#		texfile.write(' \\textbf{%.5f} ' % ( t4 ) )
		#	else:
		#		texfile.write(' %.5f ' % ( t4 ) )
		#else:
		#	texfile.write('& fail '  )
		
		
		
		texfile.write('\\\\\n')
				
	texfile.write('\\hline\n')		  	
	texfile.write('\\end{tabular}\n')
	texfile.write('}\n')		
	texfile.write('\\end{table}\n')
	
	texfile.write('\\end{center}\n') 
	

	
	
	texfile.write('\\end{document}\n')
