#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <iostream>
#include "bitmap.h"

#define MAX_VERTEX 20000

using namespace std;

int Vnbr,Enbr;
word bit[MAX_VERTEX][NWORDS(MAX_VERTEX)];
word pbit[MAX_VERTEX][NWORDS(MAX_VERTEX)];
int degree[MAX_VERTEX];
int pos[MAX_VERTEX];
int rec[MAX_VERTEX];
int record;
int set[MAX_VERTEX];
int level;
long long int subp;
clock_t clk;
int timeout;
double elapsed;
int s = 2;
int cnt = 0;

void make_saturated_list(int * set, int size, word * U, int W, int * nncnt, int * saturated_vertex, int & saturated_size){
	int u = set[size-1];
	saturated_size = 0;

	word R[NWORDS(Vnbr)];

	for(int k=0; k <= W; k++){
		R[k] = U[k];
	}

	// Complexity O(|P'|)
	for(int j = 0; j < size-1; j++){
		int v = set[j];
		if( !test(bit[u],v) ){
			nncnt[v]++;
			#ifdef DEBUG
			printf("update nncnt[%d] = %d\n",v, nncnt[v]);
			#endif
		}
	}

	int iv = 0, jv = LSB(R, iv, W);

	while(jv >= 0){
		int v = indexbit(iv, jv);
		
		if(!test(bit[u], v)){
			nncnt[v]++;
		}

		resetbit(R, v);
		jv = LSB(R, iv, W);
	}

	for(int j = 0; j < size; j++){
		int v = set[j];
		if( nncnt[v] == s-1){
			saturated_vertex[saturated_size++] = v;
		}
	}
}

bool isPlex(int * S, int satured_size, int v){
	for(int i=0; i<satured_size; i++){
		int u = S[i];

		if(!test(bit[v], u)) return false;
	}

	return true;
}


void geracao(word * U, int W, int level, word * newU, int * nncnt){
	word R[NWORDS(Vnbr)];
	for(int i=0; i<=W; i++){ 
		R[i] = U[i]; 
		newU[i] = 0LL;
	}

	int iv, jv;

	int S[Vnbr];
	int satured_size = 0;

	make_saturated_list(set, level, U, W, nncnt, S, satured_size);

	iv = 0;
	jv = LSB(R, iv, W);

	while(jv >= 0){
		int v = indexbit(iv, jv);

		if(nncnt[v] <= s-1 && isPlex(S, satured_size, v)){
			setbit(newU, v);
		}

		resetbit(R, v);
		jv = LSB(R, iv, W);
	}
}

int readFile(FILE * graphFile, int & n, int  & m) {
	char type  = ' ';
	char linestr[1024];
	char * datastr;
	long i, j;
	int nedges;

	//printf("Reading graph...\n");

	while (type != 'p') {
		type = fgetc(graphFile);
		if (type != EOF) {
			/* header */
			if (type == 'c') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;
			}

			/* Vertices */
			if (type == 'p') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;

				datastr = strtok(linestr," ");

				datastr = strtok(NULL," ");
				n = atoi(datastr);

				datastr = strtok(NULL," ");
				m = atoi(datastr);

			}
		}
	}

	printf("Graph with %d vertices and %d edges density %f\n", n, m, (double)(2*m)/(n*(n-1)) );


  	for(i=0; i<n; i++) degree[i] = 0;
  	for(i=0;i<n;i++)
		for(j=0;j<n/INT_SIZE+1;j++)
  			bit[i][j] = 0;

  	nedges = 0;
	type = fgetc(graphFile);
	while (type != EOF) {
		/* Edges */
		if (type == 'e') {
			datastr = fgets(linestr, 100, graphFile);


			if (datastr == NULL)
				return -1;

			datastr = strtok(linestr," ");
			i = atol(datastr) - 1;

			datastr = strtok(NULL," ");
			j = atol(datastr) - 1;

			//printf("edge %d %d\n", i , j);



  			if( i != j ){
				nedges++;
				setbit(bit[i], j);
				setbit(bit[j], i);
				degree[i]++;
				degree[j]++;
  			}

		}
		type = fgetc(graphFile);
	}

	int contador = 0;

	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			if( test(bit[i],j) ) contador++;
		}
	}

	contador = contador / 2;


	if( contador != m ){
		printf("Error bit adjacency graph representation\n");
		exit(1);
	}

	return 0;
}

void degree_order(){
	int degree[MAX_VERTEX];
	int used[MAX_VERTEX];
	int count;
	int delta = 0;

	/* order vertices */
	for(int i=0;i<Vnbr;i++) {
		degree[i] = 0;
		for(int j=0;j<Vnbr;j++)
  			if ( test(bit[i],j) ) degree[i]++;
			if( degree[i] > delta) delta = degree[i];
  	}

  	for(int i=0;i<Vnbr;i++)
		used[i] = false;

  	count = Vnbr - 1;
  	do{
 		int min_degree;
 		int p;


 		min_degree = delta + 1;
 		p = -1;

 		for(int i=Vnbr-1;i>=0;i--){
   			if((!used[i])&&(degree[i]<min_degree)){
	 			min_degree = degree[i];
	  			p = i;
   			}
 		}

 		pos[count--] = p;
 		used[p] = true;


 		for(int j=0;j<Vnbr;j++)
   			if((!used[j])&&(j!=p)&&(test(bit[p],j)))
	 			degree[j]--;

  	} while(count >= 0);

}


void branching(word * U, int W, int limite){
	word R[NWORDS(Vnbr)];
	word S[NWORDS(Vnbr)];
	int UB = 0;
	int estavel[Vnbr];
	int count = 0;

	for(int i=0; i<=W; i++){
		R[i] = U[i];
	}

	while(1){
		if(UB >= limite) return;

		int iv, jv;

		for(int k = 0; k <= W; k++){
			S[k] = R[k];
		}

		iv = 0;
		jv = LSB(S, iv, W);

		if(jv < 0) break;

		count = 0;

		while(jv >= 0){
			int v = indexbit(iv, jv);

			//if (count <= limite) resetbit(U, v);
			estavel[count++] = v;

			for(int k = 0; k <= W; k++){
				S[k] = S[k] & ~bit[v][k];
			}

			resetbit(S, v);

			jv = LSB(S, iv, W);
		}

		//UB += s > count ? count : s;
		UB++;
		for(int k = 0; k < count; k++){
			int v = estavel[k];
			resetbit(U, v);
			resetbit(R, v);
		}
	}

	return ;
}


void basicPlexBranching(word * U, int W, int level, int * nncnt){
	int iv, jv;
	int iu, ju;
	word newU [NWORDS(Vnbr)];
	word R [NWORDS(Vnbr)];


	if(level > record){
		record = level;
		for(int i = 0; i < level; i++) rec[i] = set[i];
		printf("improving solution size %d\n", record);
	}
	subp++;

	if(++cnt > 1000) {
		cnt = 0;
		elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;
		if(elapsed >= timeout) {
			printf("TIMEOUT\n");
			printf("best clique: ");
			for(int i = 0; i < record; i++){
				printf("%d ", rec[i]);
			}

			printf("\n");
		  	printf("record          =  %10d\n", record );
		  	printf("subp            =  %10lld\n", subp );
		  	printf("time            =  %10.5f\n", elapsed );
			exit(0);
		}

	}

	for(int k = 0; k <= W; k++){
		R[k] = U[k];
	}
	branching(R, W, record- level);
	iv = W;
	jv = MSB(R, iv);
	iu = W;
	ju = MSB(U, iu);
	int newnncnt[Vnbr];

	while(jv >= 0){
		int v = indexbit(iv, jv);

		for(int i=0; i<Vnbr; i++){
			newnncnt[i] = nncnt[i];
		}		
		set[level] = v;
		resetbit(R, v);
		resetbit(U, v);
		jv = MSB(R, iv);
		ju = MSB(U, iu);


		geracao(U, iu, level+1, newU, newnncnt);
		basicPlexBranching(newU, iu, level+1, newnncnt);
	}
}



int main(int argc, char *argv[]){
	FILE *infile;

  	/* read input */
  	if(argc < 3) {
		printf("Usage: BB [timeout] k infile\n");
		printf("k: k-plex size\n");
		printf("infile: input file path\n");
		exit(1);
  	}

  	timeout = atoi( argv[1] );

  	printf("timeout %d s\n", timeout );

  	if((infile=fopen(argv[3],"r"))==NULL){
		printf("Error in graph file\n %s\n", argv[2]);
		exit(0);
  	}


	if(argv[3])
		s = atoi(argv[2]);

  	/* read graph */
  	readFile(infile, Vnbr, Enbr);

  	/* "start clock" */
  	clk = clock();

  	//ordenaçao smallest-last
  	degree_order();

  	//Reconstrução da matriz adjacência
	for(int i = 0; i < Vnbr; i++){
		for(int k = 0; k < NWORDS(Vnbr); k++){
			pbit[i][k] = 0LL;
		}
	
		for(int j = 0; j < Vnbr; j++){
			if( test( bit[pos[i]], pos[j] ) ){
				setbit( pbit[i], j);
			}
		}
	}

	int contador = 0;

	for(int i = 0; i < Vnbr; i++){
		for(int j = 0; j < Vnbr; j++){
			if( test(pbit[i], j) ) contador++;
		}
	}

	contador = contador / 2;


	if( contador != Enbr){
		printf("Error bit adjacency graph representation\n");
		exit(1);
	}


  	word U[ NWORDS(Vnbr) ];
  	for(int k = 0; k < NWORDS(Vnbr); k++){
  		U[k] = 0LL;
  	}
  	for(int i = 0; i < Vnbr; i++){
		setbit(U, i);
  	}


  	subp  = 0LL;
  	printf("start search\n");


	int nncnt[Vnbr];

	for(int i = 0; i < Vnbr; i++){
		nncnt[i] = 0;
	}

	basicPlexBranching(U, high(Vnbr), 0, nncnt);

  	elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;

	printf("best clique: ");
	for(int i = 0; i < record; i++){
		printf("%d ", rec[i]);
	}

	printf("\n");
  	printf("record          =  %10d\n", record );
  	printf("subp            =  %10lld\n", subp );
  	printf("time            =  %10.5f\n", elapsed );
  	//printf("cont1         =  %10lld\n", cont1 );
  	//printf("cont2         =  %10lld\n", cont2 );
  	//printf("cont3         =  %10lld\n", cont3 );
  	//printf("cont4         =  %10lld\n", cont4 );

	return 0;
}
