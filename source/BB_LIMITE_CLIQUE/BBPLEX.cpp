#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <iostream>
#include "bitmap.h"

#define MAX_VERTEX 20000

using namespace std;

int Vnbr,Enbr;
word bit[MAX_VERTEX][NWORDS(MAX_VERTEX)];
word pbit[MAX_VERTEX][NWORDS(MAX_VERTEX)];
int degree[MAX_VERTEX];
int pos[MAX_VERTEX];
int rec[MAX_VERTEX];
int record;
int set[MAX_VERTEX];
int level;
long long int subp;
clock_t clk;
int timeout;
double elapsed;
int s = 2;
int cnt = 0;



int readFile(FILE * graphFile, int & n, int  & m) {
	char type  = ' ';
	char linestr[1024];
	char * datastr;
	long i, j;
	int nedges;

	//printf("Reading graph...\n");

	while (type != 'p') {
		type = fgetc(graphFile);
		if (type != EOF) {
			/* header */
			if (type == 'c') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;
			}

			/* Vertices */
			if (type == 'p') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;

				datastr = strtok(linestr," ");

				datastr = strtok(NULL," ");
				n = atoi(datastr);

				datastr = strtok(NULL," ");
				m = atoi(datastr);

			}
		}
	}

	printf("Graph with %d vertices and %d edges density %f\n", n, m, (double)(2*m)/(n*(n-1)) );


  	for(i=0; i<n; i++) degree[i] = 0;
  	for(i=0;i<n;i++)
		for(j=0;j<n/INT_SIZE+1;j++)
  			bit[i][j] = 0;

  	nedges = 0;
	type = fgetc(graphFile);
	while (type != EOF) {
		/* Edges */
		if (type == 'e') {
			datastr = fgets(linestr, 100, graphFile);


			if (datastr == NULL)
				return -1;

			datastr = strtok(linestr," ");
			i = atol(datastr) - 1;

			datastr = strtok(NULL," ");
			j = atol(datastr) - 1;

			//printf("edge %d %d\n", i , j);



  			if( i != j ){
				nedges++;
				setbit(bit[i], j);
				setbit(bit[j], i);
				degree[i]++;
				degree[j]++;
  			}

		}
		type = fgetc(graphFile);
	}

	int contador = 0;

	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			if( test(bit[i],j) ) contador++;
		}
	}

	contador = contador / 2;


	if( contador != m ){
		printf("Error bit adjacency graph representation\n");
		exit(1);
	}

	return 0;
}

void degree_order(){
	int degree[MAX_VERTEX];
	int used[MAX_VERTEX];
	int count;
	int delta = 0;

	/* order vertices */
	for(int i=0;i<Vnbr;i++) {
		degree[i] = 0;
		for(int j=0;j<Vnbr;j++)
  			if ( test(bit[i],j) ) degree[i]++;
			if( degree[i] > delta) delta = degree[i];
  	}

  	for(int i=0;i<Vnbr;i++)
		used[i] = false;

  	count = Vnbr - 1;
  	do{
 		int min_degree;
 		int p;


 		min_degree = delta + 1;
 		p = -1;

 		for(int i=Vnbr-1;i>=0;i--){
   			if((!used[i])&&(degree[i]<min_degree)){
	 			min_degree = degree[i];
	  			p = i;
   			}
 		}

 		pos[count--] = p;
 		used[p] = true;


 		for(int j=0;j<Vnbr;j++)
   			if((!used[j])&&(j!=p)&&(test(bit[p],j)))
	 			degree[j]--;

  	} while(count >= 0);


}




//bool isPlex(word * C, int k, int W, int size_C){
//	int fs, ls;
//	int high_W = high(W);
//	fs = LSB2(C, 0, high_W);
//	ls = MSB2(C, high_W);
//
//	if(fs < 0 || ls < 0) return false;
//
//	int degree[W];
//	word d[NWORDS(W)];
//	for(int i=fs; i<=ls; i++) {o
//
//		if(!test(C, i))	continue;
//
//		for(int l=0; l<=high_W; l++) d[l] = 0LL;
//
//		for(int j = high(fs); j <= high(ls); j++) d[j] = bit[i][j] & C[j];
//
//		degree[i] = COUNT(d, high(ls));
// 	}
//
//	for(int v = fs; v <= ls; v++){
//		if(test(C, v) && degree[v] < size_C - k){
//			return false;
//		}
//	}
//	return true;
//}
//
//bool isPlex2(word *C, int s){
//	word R[NWORDS(Vnbr)];
//	word S[NWORDS(Vnbr)];
//
//	int W = high(Vnbr);
//	int sizeC = COUNT(C, W);
//
//	for(int k = 0; k <= W; k++) R[k] = C[k];
//
//	int iv = 0;
//	int jv = LSB(R, iv, W);
//
//	while ( jv >= 0 ){
//		int v = indexbit(iv, jv);
//
//		for(int k = 0; k <= W; k++) S[k] = C[k] & bit[v][k];
//
//		if( COUNT(S, W)  < sizeC - s) return false;
//
//		resetbit(R, v);
//
//		jv = LSB(R, iv, W);
//
//	}
//
//	return true;
//
//}

bool isPlex3(int level, int s){
	word C[NWORDS(Vnbr)];

	int W = high(Vnbr);

	for(int i=0; i<=W; i++) C[i] = 0LL;

	for(int i=0; i<level; i++) setbit(C, set[i]);

	//word R[W+1];
	word S[W+1];

	for(int i=0; i<level; i++){
			int v = set[i];

			for(int k = 0; k <= W; k++) S[k] = C[k] & bit[v][k];

			if( COUNT(S, W)  < level - s) return false;

	}
	return true;
}

bool isPlex(int * set, int level, int s){
	word C[NWORDS(Vnbr)];

	int W = high(Vnbr);

	for(int i=0; i<=W; i++) C[i] = 0LL;

	for(int i=0; i<level; i++) setbit(C, set[i]);

	//word R[W+1];
	word S[W+1];

	//printf("level %d\n", level );


	for(int i=0; i<level; i++){
		int v = set[i];

		for(int k = 0; k <= W; k++) S[k] = C[k] & bit[v][k];

		//printf("testa vertice %d(%d)\n", v, COUNT(S,W) );

		if( COUNT(S, W)  < level - s) return false;
		//for(int i = 0; i < Vnbr; i++){
		//
		//	for(int k = 0; k < NWORDS(Vnbr); k++){
		// 		pbit[i][k] = 0LL;
		//	}
		//
		//	for(int j = 0; j < Vnbr; j++){
		//		if( test( bit[pos[i]], pos[j] ) ){
		//			setbit( pbit[i], j);
		//		}
		//	}
		//}
	}

	return true;
}

bool coloring(word * U, int W, int limite){
	word R[W+1];
	word S[W+1];
	int UB = 0;
	int estavel[Vnbr];
	int count = 0;

	for(int i=0; i<=W; i++){
		R[i] = U[i];
	}

	while(1){
		if(UB == limite) return true;

		int iv, jv;

		for(int k = 0; k <= W; k++){
			S[k] = R[k];
		}

		iv = 0;
		jv = LSB(S, iv, W);

		if(jv < 0) break;

		count = 0;

		while(jv >= 0){
			int v = indexbit(iv, jv);

			//if (count <= limite) resetbit(U, v);
			estavel[count++] = v;

			for(int k = 0; k <= W; k++){
				S[k] = S[k] & ~bit[v][k];
			}

			resetbit(S, v);
				jv = LSB(S, iv, W);
			}

			UB++;
			for(int k = 0; k < count; k++){
				int v = estavel[k];
				//resetbit(U, v);
				resetbit(R, v);
		}
	}

	return false;
}

void branching(word * U, int W, int limite){
	word R[W+1];
	word S[W+1];
	int UB = 0;
	int estavel[Vnbr];
	int count = 0;

	for(int i=0; i<=W; i++){
		R[i] = U[i];
	}

	while(1){
		if(UB == limite) return;

		int iv, jv;

		for(int k = 0; k <= W; k++){
			S[k] = R[k];
		}

		iv = 0;
		jv = LSB(S, iv, W);

		if(jv < 0) break;

		count = 0;

		while(jv >= 0){
			int v = indexbit(iv, jv);

			//if (count <= limite) resetbit(U, v);
			estavel[count++] = v;

			for(int k = 0; k <= W; k++){
				S[k] = S[k] & ~bit[v][k];
			}

			resetbit(S, v);

			jv = LSB(S, iv, W);
		}

		UB++;
		for(int k = 0; k < count; k++){
			int v = estavel[k];
			resetbit(U, v);
			resetbit(R, v);
		}
	}

	return ;
}

void geracao(word * U, int W, int level, word * newU){
	word R[W+1];
	for(int i=0; i<=W; i++) R[i] = U[i];
	for(int i=0; i<=W; i++) newU[i] = 0LL;

	int iv, jv;

	iv = 0;
	jv = LSB(R, iv, W);

	//printf("newU\n");
	while(jv >= 0){
		int v = indexbit(iv, jv);

		set[level+1] = v;
		if(isPlex(set, level+2, s)){
			//printf("vertex %d\n", v);
			setbit(newU, v);
		}

		resetbit(R, v);
		jv = LSB(R, iv, W);
	}
	//printf("]\n");
}


void basicPlex(word * U, int W, int level){
	int iv, jv;
	int sizeU = COUNT(U, W);
	word newU [W+1];
	if(level > record){
		record = level;
		for(int i = 0; i < level; i++) rec[i] = set[i];
		printf("improving solution size %d\n", record );
	}
	subp++;
	//if(!coloring(U, W, record-level)) return;

	iv = W;
	jv = MSB(U, iv);

	//printf("level %d size U %d\n", level, sizeU);

	while(jv >= 0){
		if(sizeU + level <= record) return ;

		int v = indexbit(iv, jv);

		//printf("branching vertex %d\n", v);

		set[level] = v;

		//if( !isPlex(set, level+1, s) ) { printf("is not kplex\n"); exit(1); }

		resetbit(U, v);

		jv = MSB(U, iv);

		//printf("generation newU\n");

		geracao(U, iv, level, newU);

		//printf("size newU %d\n", COUNT(newU, iv));
		basicPlex(newU, iv, level+1);
		sizeU--;

	}
}

void basicPlexColoring(word * U, int W, int level){
	int iv, jv;
	//int sizeU = COUNT(U, W);
	word newU [W+1];
	if(level > record){
		record = level;
		for(int i = 0; i < level; i++) rec[i] = set[i];
		printf("improving solution size %d\n", record );
	}
	subp++;
	if(!coloring(U, W, record-level)) return;

	iv = W;
	jv = MSB(U, iv);

	//printf("level %d size U %d\n", level, sizeU);

	while(jv >= 0){
		// if(sizeU + level <= record) return ;

		int v = indexbit(iv, jv);

		//printf("branching vertex %d\n", v);

		set[level] = v;

		//if( !isPlex(set, level+1, s) ) { printf("is not kplex\n"); exit(1); }

		resetbit(U, v);

		jv = MSB(U, iv);

		//printf("generation newU\n");

		geracao(U, iv, level, newU);

		//printf("size newU %d\n", COUNT(newU, iv));
		basicPlexColoring(newU, iv, level+1);
		//sizeU--;
	}
}

void basicPlexBranching(word * U, int W, int level){
	int iv, jv;
	int iu, ju;
	//int sizeU = COUNT(U, W);
	word newU [W+1];
	word R [W+1];
	if(level > record){
		record = level;
		for(int i = 0; i < level; i++) rec[i] = set[i];
		printf("improving solution size %d\n", record );
	}
	subp++;
	for(int k = 0; k <= W; k++){
		R[k] = U[k];
	}
	branching(R, W, record- level);
	iv = W;
	jv = MSB(R, iv);
	iu = W;
	ju = MSB(U, iu);
	//printf("level %d size U %d\n", level, sizeU);
	while(jv >= 0){
		//if(sizeU + level <= record) return ;
		int v = indexbit(iv, jv);

		//printf("branching vertex %d\n", v);
		set[level] = v;
		//if( !isPlex(set, level+1, s) ) { printf("is not kplex\n"); exit(1); }
		resetbit(R, v);
		resetbit(U, v);
		jv = MSB(R, iv);
		ju = MSB(U, iu);


		//printf("generation newU\n");
		geracao(U, iu, level, newU);
		//printf("size newU %d\n", COUNT(newU, iv));
		basicPlexBranching(newU, iu, level+1);
		//sizeU--;
	}
}

bool isCoKPlex(word * C, int s){

	int W = high(Vnbr);
	word R[W+1];

	for(int i=0; i<=W; i++) R[i] = C[i];
	int iv, jv;

	word S[W+1];

	iv = 0;
	jv = LSB(R, iv, W);

	while(jv >= 0){
		int v = indexbit(iv, jv);

		for(int k = 0; k <= W; k++) S[k] = C[k] & bit[v][k];

		if(COUNT(S, W)  >= s) return false;
		
		resetbit(R, v);
		
		jv = LSB(R, iv, W);
	}
	
	return true;
}

int menor(int a, int b){ return a < b ? a : b;}

int cocoloring(word * U, int W){
	word R[Vnbr][W+1];
	word S[W+1];
	int l[Vnbr];
	for(int i=0; i<Vnbr; i++) l[i] = 0x7FFFFFFF-s;

	for(int i=0; i<Vnbr; i++){
		for(int j=0; j<=W; j++){
			R[i][j] = 0LL;
		}
	}

	for(int i=0; i<=W; i++){
		S[i] = U[i];
	}

	int iv, jv;
	iv = 0;
	jv = LSB(S, iv, W);

	int max = -1;

	while(jv >= 0){
		int v = indexbit(iv, jv);

		for(int i=0; i<Vnbr; i++){
			setbit(R[i], v);
			if(isCoKPlex(R[i], s)){
				if(i > max) max = i;
					break;
			}else{
				resetbit(R[i], v);
			}
		}
		resetbit(S, v);
		jv = LSB(S, iv, W);
	}
	int iv2, jv2;
	int max_degree[max+1];
	for(int i=0; i<=max; i++) max_degree[i] = 0;

	for(int i = max; i>=0; i--){

		int m = COUNT(R[i], W);

		for(int j=m; j>=0; j--){
			word b[W+1];
	
			for(int k=0; k<=W; k++) b[k] = R[i][k];
			iv2 = 0;
			jv2 = MSB(b, iv2);
			int cont = 0;

			while(jv2 >= 0){
				int v = indexbit(iv2, jv2);
		
				for(int k = 0; k <= W; k++) S[k] = R[i][k] & bit[v][k];
			
				int d = COUNT(S, W);
				
				if(d > max_degree[j]) max_degree[j] = d;
				if(d >= m) cont++;
	
				resetbit(b, v);
				jv2 = MSB(b, iv2);
			}

			if(cont >= s+m){
				l[i] = m;
				break;
			}
		}
	}

	int bound = 0;
	for(int i=0; i<=max; i++){
		bound += menor(COUNT(R[i], W), menor(2*s-2+2%2, menor(s+l[i], max_degree[i]+s)));
	}

	return bound;
}


void basicPlexCocoloring(word * U, int W, int level){
	int iv, jv;
	int iu, ju;
	//int sizeU = COUNT(U, W);
	word newU [W+1];
	word R [W+1];

	if(level > record){
		record = level;
		for(int i = 0; i < level; i++) rec[i] = set[i];
		printf("improving solution size %d\n", record );
	}

	subp++;

	for(int k = 0; k <= W; k++){
		R[k] = U[k] ;
	}


	int bound = cocoloring(R, W);
	if(bound < record - level){
		return;
	}


	iv = W;
	jv = MSB(R, iv);


	iu = W;
	ju = MSB(U, iu);
	//printf("level %d size U %d\n", level, sizeU);

	while(jv >= 0){
		//if(sizeU + level <= record) return ;

		int v = indexbit(iv, jv);

		//printf("branching vertex %d\n", v);

		set[level] = v;

		//if( !isPlex(set, level+1, s) ) { printf("is not kplex\n"); exit(1); }

		resetbit(R, v);

		resetbit(U, v);


		jv = MSB(R, iv);

		ju = MSB(U, iu);


		//printf("generation newU\n");



		geracao(U, iu, level, newU);

		//printf("size newU %d\n", COUNT(newU, iv));
		basicPlexCocoloring(newU, iu, level+1);
		//sizeU--;

	}
}


int cocoloringBranching(word * U, int W, int level){
	word R[Vnbr][W+1];
	word S[W+1];

	int T[Vnbr];

	for(int i=0; i<Vnbr; i++){
		for(int j=0; j<=W; j++){
			R[i][j] = 0LL;
		}

		T[i] = 0;
	}

	for(int i=0; i<=W; i++){
		S[i] = U[i];
	}


	int iv, jv;
	iv = 0;
	jv = LSB(S, iv, W);

	int max = -1;

	while(jv >= 0){
		int v = indexbit(iv, jv);

		for(int i=0; i<Vnbr; i++){
			setbit(R[i], v);
			if(isCoKPlex(R[i], s)){
				T[i]++;
				if(i > max) max = i;
				break;
			}else{
				resetbit(R[i], v);
			}
		}
		resetbit(S, v);
		jv = LSB(S, iv, W);
	}
	
	int l1 = 2*s-2+s%2;

	int bound = 0;

	for(int i=0; i<=max; i++){
		int max_d = 0;
		word P[W+1];

		for(int j=0; j<W; j++){
			P[j] = R[i][j];
		}

		for(iv = 0, jv = LSB(P, iv, W); jv >= 0; jv = LSB(P, iv, W)){
			int v = indexbit(iv, jv);
			
			for(int k = 0; k <= W; k++) S[k] = R[i][k] & bit[v][k];
			
			int d = COUNT(S, W);

			if(d > max_d) max_d = d;
			
			resetbit(P, v);
		}

		bound += menor(l1, menor(T[i], max_d));

		if(bound < level){
			for(int j = 0; j<W; j++){
				U[j] = U[j] & ~R[i][j];
			}
		}
	}	
}


void basicPlexCocoloringBranching(word * U, int W, int level){
	int iv, jv;
	int iu, ju;
	//int sizeU = COUNT(U, W);
	word newU [W+1];
	word R [W+1];

	if(level > record){
		record = level;
		for(int i = 0; i < level; i++) rec[i] = set[i];
		printf("improving solution size %d\n", record );
	}

	subp++;

	for(int k = 0; k <= W; k++){
		R[k] = U[k] ;
	}


	cocoloringBranching(R, W, record - level);

	iv = W;
	jv = MSB(R, iv);


	iu = W;
	ju = MSB(U, iu);
	//printf("level %d size U %d\n", level, sizeU);

	while(jv >= 0){
		//if(sizeU + level <= record) return ;

		int v = indexbit(iv, jv);

		//printf("branching vertex %d\n", v);

		set[level] = v;

		//if( !isPlex(set, level+1, s) ) { printf("is not kplex\n"); exit(1); }

		resetbit(R, v);

		resetbit(U, v);


		jv = MSB(R, iv);

		ju = MSB(U, iu);


		//printf("generation newU\n");



		geracao(U, iu, level, newU);

		//printf("size newU %d\n", COUNT(newU, iv));
		basicPlexCocoloringBranching(newU, iu, level+1);
		//sizeU--;

	}
}


int main(int argc, char *argv[]){
	FILE *infile;

  	/* read input */
  	if(argc < 4) {
		printf("Usage: BB [timeout] algorithm k infile\n");
		printf("algorithm: 1 for Branching, 2 for Coloring or 3 for Cocoloring\n");
		printf("k: k-plex size\n");
		printf("infile: input file path\n");
		exit(1);
  	}

  	timeout = atoi( argv[1] );

  	printf("timeout %d s\n", timeout );

  	if((infile=fopen(argv[4],"r"))==NULL){
		printf("Error in graph file\n %s\n", argv[2]);
		exit(0);
  	}


	if(argv[3])
		s = atoi(argv[3]);

  	/* read graph */
  	readFile(infile, Vnbr, Enbr);

  	/* "start clock" */
  	clk = clock();

  	//ordenaçao smallest-last
  	//degree_order();

  	//Reconstrução da matriz adjacência
	/*for(int i = 0; i < Vnbr; i++){
		for(int k = 0; k < NWORDS(Vnbr); k++){
			pbit[i][k] = 0LL;
		}
	
		for(int j = 0; j < Vnbr; j++){
			if( test( bit[pos[i]], pos[j] ) ){
				setbit( pbit[i], j);
			}
		}
	}*/

  	word U[ NWORDS(Vnbr) ];
  	for(int k = 0; k < NWORDS(Vnbr); k++){
  		U[k] = 0LL;
  	}
  	for(int i = 0; i < Vnbr; i++){
		setbit(U, i);
  	}


  	subp  = 0LL;
  	printf("start search\n");


	switch(atoi(argv[2])){
		case 1:
			basicPlexBranching(U, high(Vnbr), 0);
			break;
		case 2:
			basicPlexColoring(U, high(Vnbr), 0);
			break;
		case 3:
			basicPlexCocoloring(U, high(Vnbr), 0);
			break;
		case 4:
			basicPlexCocoloringBranching(U, high(Vnbr), 0);
			break;
		default:
			printf("Invalid Algorithm.\n");
	}	
  	//printf("Cocoloring %d", cocoloring(U, high(Vnbr)));

  	elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;


	printf("best clique: ");
	for(int i = 0; i < record; i++){
		printf("%d ", rec[i]);
	}

	printf("\n");
  	printf("record          =  %10d\n", record );
  	printf("subp            =  %10lld\n", subp );
  	printf("time            =  %10.5f\n", elapsed );
  	//printf("cont1           =  %10lld\n", cont1 );
  	//printf("cont2           =  %10lld\n", cont2 );
  	//printf("cont3           =  %10lld\n", cont3 );
  	//printf("cont4           =  %10lld\n", cont4 );

	return 0;
}
