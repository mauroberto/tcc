# *k*-Plex Maximum Algorithm

**Usage**:

	./BB timeout algorithm k infile
	
	e.g.: ./BB 3600 1 2 brock200_2.clq
	
**Arguments:**	

Arg | Value
--- | ---
algorithm | ```1``` for Branching, ```2``` for Coloring or ```3``` for Cocoloring.
k | k-plex size. ```e.g.: 2```.
infile | input file path.
timeout | timeout value in seconds. ```e.g.: 3600```. 

