import pickle


instancias = [
("brock200_1",200,14834),
("brock200_2",200,9876),
("brock200_3",200,12048),
("brock200_4",200,13089),
("brock400_1",400,59723),
("brock400_2",400,59786),
("brock400_3",400,59681),
("brock400_4",400,59765),
("brock800_1",800,207505),
("brock800_2",800,208166),
("brock800_3",800,207333),
("brock800_4",800,207643),
("C125.9",125,6963),
("C250.9",250,27984),
("C500.9",500,112332),
("C1000.9",1000,450079),
("C2000.5",2000,999836),
("C2000.9",2000,1799532),
("C4000.5",4000,4000268),
("c-fat200-1",200,1534),
("c-fat200-2",200,3235),
("c-fat200-5",200,8473),
("c-fat500-1",500,4459),
("c-fat500-2",500,9139),
("c-fat500-5",500,23191),
("c-fat500-10",500,46627),
#("DSJC500_5",500,125248),
#("DSJC1000_5",1000,499652),
("gen200_p0.9_44",200,17910),
("gen200_p0.9_55",200,17910),
("gen400_p0.9_55",400,71820),
("gen400_p0.9_65",400,71820),
("gen400_p0.9_75",400,71820),
("hamming6-2",64,1824),
("hamming6-4",64,704),
("hamming8-2",256,31616),
("hamming8-4",256,20864),
("hamming10-2",1024,518656),
("hamming10-4",1024,434176),
("johnson8-2-4",28,210),
("johnson8-4-4",70,1855),
("johnson16-2-4",120,5460),
("johnson32-2-4",496,107880),
("keller4",171,9435),
("keller5",776,225990),
#("keller6",3361,4619898),
("MANN_a9",45,918),
("MANN_a27",378,70551),
("MANN_a45",1035,533115),
#("MANN_a81",3321,5506380),
("p_hat300-1",300,10933),
("p_hat300-2",300,21928),
("p_hat300-3",300,33390),
("p_hat500-1",500,31569),
("p_hat500-2",500,62946),
("p_hat500-3",500,93800),
("p_hat700-1",700,60999),
("p_hat700-2",700,121728),
("p_hat700-3",700,183010),
("p_hat1000-1",1000,122253),
("p_hat1000-2",1000,244799),
("p_hat1000-3",1000,371746),
("p_hat1500-1",1500,284923),
("p_hat1500-2",1500,568960),
("p_hat1500-3",1500,847244),
("san200_0.7_1",200,13930),
("san200_0.7_2",200,13930),
("san200_0.9_1",200,17910),
("san200_0.9_2",200,17910),
("san200_0.9_3",200,17910),
("san400_0.5_1",400,39900),
("san400_0.7_1",400,55860),
("san400_0.7_2",400,55860),
("san400_0.7_3",400,55860),
("san400_0.9_1",400,71820),
("san1000",1000,250500),
("sanr200_0.7",200,13868),
("sanr200_0.9",200,17863),
("sanr400_0.5",400,39984),
("sanr400_0.7",400,55869)
]


with open('branching.tex', 'w') as texfile:
	for i in range(len(instancias)):
		texfile.write(str(i+1))
		texfile.write(";")

	texfile.write("\n")
	for inst in instancias:
		filename = "COLORING/out2/%s.out" % (inst[0])
		f = open(filename)
		lines = f.readlines();
		t = len(lines)								
		r1   = int(lines[t-3].split("=")[1])
		t1   = float(lines[t-1].split("=")[1])
		sub1 = int(lines[t-2].split("=")[1])	
		w1 = str(r1)
		texfile.write(w1)
		texfile.write(";")
	texfile.write("\n")
	for inst in instancias:
		filename = "COLORING/out2/%s.out" % (inst[0])
		f = open(filename)
		lines = f.readlines();
		t = len(lines)								
		r1   = int(lines[t-3].split("=")[1])
		t1   = float(lines[t-1].split("=")[1])
		sub1 = int(lines[t-2].split("=")[1])	
		w1 = str(t1)
		texfile.write(w1)
		texfile.write(";")

with open('iwcch.tex', 'w') as texfile:
	for i in range(len(instancias)):
		texfile.write(" ")
		texfile.write(";")

	texfile.write("\n")
	for inst in instancias:
		filename = "COCOLORING/out2/%s.out" % (inst[0])
		f = open(filename)
		lines = f.readlines();
		t = len(lines)								
		r1   = int(lines[t-3].split("=")[1])
		t1   = float(lines[t-1].split("=")[1])
		sub1 = int(lines[t-2].split("=")[1])	
		w1 = str(r1)
		texfile.write(w1)
		texfile.write(";")
	texfile.write("\n")
	for inst in instancias:
		filename = "COCOLORING/out2/%s.out" % (inst[0])
		f = open(filename)
		lines = f.readlines();
		t = len(lines)								
		r1   = int(lines[t-3].split("=")[1])
		t1   = float(lines[t-1].split("=")[1])
		sub1 = int(lines[t-2].split("=")[1])	
		w1 = str(t1)
		texfile.write(w1)
		texfile.write(";")


with open('projects.tex', 'w') as texfile:
	texfile.write('\\setlength\\LTleft{0pt}            % default: \\fill\n')
	texfile.write('\\setlength\\LTright{0pt}           % default: \\fill\n')
	texfile.write('\\begin{longtable}{@{\extracolsep{\\fill}}lllll@{}}\n')	
	texfile.write('\\caption{Resultados dos algoritmos de \\textit{Branching} e \\textit{IWCCH}}\\ \n')

	texfile.write('\\hline\n')
	texfile.write('Grafo & Branching & & IWCCH & \\\\ \n')
	texfile.write('\\hline\n')
	texfile.write('& $\\omega_{2}(G)$ & segundos & $\\omega_{2}(G)$ & segundos \\\\ \n')	
	texfile.write('\\label{table:table1}\\');

	texfile.write('\\hline\n')	
	
	for inst in instancias:
		
		inst_name = inst[0].replace("_","\_")
		
		texfile.write(' %s ' % ( inst_name ) )
		
		#texfile.write(' & (%d,%.3f)' % (inst[1], inst[2]))
		
		flag1 = True
		
		menor = 3600
		w = -1
		
		try:
			filename = "COLORING/out2/%s.out" % (inst[0])
			f = open(filename)
			lines = f.readlines();
			t = len(lines)								
			r1   = int(lines[t-3].split("=")[1])
			t1   = float(lines[t-1].split("=")[1])
			sub1 = int(lines[t-2].split("=")[1])	
			w1 = r1
		except:
			flag1 = False
		
		
		if flag1:
			if t1 < menor:
				menor  = t1
		
		flag2 = True
		
		try:
			filename = "COCOLORING/out2/%s.out" % (inst[0])
			f = open(filename)
			lines = f.readlines();
			t = len(lines)								
			r2   = int(lines[t-3].split("=")[1])
			t2   = float(lines[t-1].split("=")[1])
			sub2 = int(lines[t-2].split("=")[1])	
			w2 = r2
		except:
			flag2 = False
		
		if flag2:
			if t2 < menor:
				menor  = t2
		
		
		#flag3 = True
		
		#try:
		#	filename = "out4/%s.out" % (inst[0])
		#	f = open(filename)
		#	lines = f.readlines();
		#	t = len(lines)				
		#	r3   = int(lines[t-3].split("=")[1])
		#	t3   = float(lines[t-1].split("=")[1])
		#	sub3 = int(lines[t-2].split("=")[1])	
		#	w3 = r3
		#except:
		#	flag3 = False
		
		#if flag3:
		#	if t3 < menor:
		#		menor  = t3
		
		#razao1 = menor
		
		#flag4 = True
		
		
		#try:
		#	filename = "../DBB/resultado/%s.out" % (inst[0])
		#	f = open(filename)
		#	lines = f.readlines();				
		#	r4   = int(lines[0].split("=")[1])
		#	t4   = float(lines[2].split("=")[1])
		#	sub4 = int(lines[3].split("=")[1])	
		#	if w != -1:
		#		if r4 != w:
		#			print "ERRO\n"
		#	
		#	w = r4
		#except:
		#	flag4 = False
		
		#if flag4:
		#	if t4 < menor:
		#		menor  = t4
		
		
		#if flag4 and flag2:
		#	if r4 != r2:
		#		print "ERRO\n"
		
		#razao2 = t4
		
		#if flag3 and flag4:
		#	if t3 < t4:
		#		razao2 = t3
		#	else:
		#		razao2 = t4
			
		
		#texfile.write('& %d ' % ( w ) )
		
		if flag1:
			texfile.write('& %d ' % ( r1 ) )
			texfile.write('& %.5f ' % ( t1 ) )

			#texfile.write('& %d ' % ( sub1 ) )
		else:
			texfile.write('& fail '  )
		
		if flag2:
			
			texfile.write('& %d ' % ( r2 ) )
			texfile.write('& %.5f ' % ( t2 ) )
			#texfile.write('& %d ' % ( sub2 ) )
		else:
			texfile.write('& fail '  )
		
		#if flag3:			
			
		#	texfile.write('& %d ' % ( r3 ) )
		#	texfile.write('& %.5f ' % ( t3 ) )
			#texfile.write('& %d ' % ( sub3 ) )
		#else:
		#	texfile.write('& fail '  )
		
		
		#if flag4:
		#	
		#	texfile.write('& ')
		#	k = razao1/t4			
		#	if k > 4.0 and k < 16:
		#		texfile.write('$\\bigstar$')
		#	elif k >= 16 and k < 64:
		#		texfile.write('$\\bigstar\\bigstar$')
		#	elif k >= 64 and k < 256:
		#		texfile.write('$\\bigstar\\bigstar\\bigstar$')
		#	elif k >= 256 and k < 1024:
		#	    texfile.write('$\\bigstar\\bigstar\\bigstar\\bigstar$')
		#	elif k >= 1024:
		#	    texfile.write('$\\bigstar\\bigstar\\bigstar\\bigstar\\bigstar$')
		#	
			
		#	if t4 <= razao1:
		#		texfile.write(' \\textbf{%.5f} ' % ( t4 ) )
		#	else:
		#		texfile.write(' %.5f ' % ( t4 ) )
		#else:
		#	texfile.write('& fail '  )
		
		
		
		texfile.write('\\\\\n')
				
	texfile.write('\\hline\n')		  	
	texfile.write('\end{longtable}\n')
	

