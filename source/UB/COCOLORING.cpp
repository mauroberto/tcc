#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <iostream>
#include "bitmap.h"

#define MAX_VERTEX 20000

using namespace std;

int Vnbr,Enbr;
word bit[MAX_VERTEX][NWORDS(MAX_VERTEX)];
word pbit[MAX_VERTEX][NWORDS(MAX_VERTEX)];
int degree[MAX_VERTEX];
int pos[MAX_VERTEX];
int rec[MAX_VERTEX];
int record;
int set[MAX_VERTEX];
int level;
long long int subp;
clock_t clk;
int timeout;
double elapsed;
int s = 2;
int cnt = 0;

int readFile(FILE * graphFile, int & n, int  & m) {
	char type  = ' ';
	char linestr[1024];
	char * datastr;
	long i, j;
	int nedges;

	//printf("Reading graph...\n");

	while (type != 'p') {
		type = fgetc(graphFile);
		if (type != EOF) {
			/* header */
			if (type == 'c') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;
			}

			/* Vertices */
			if (type == 'p') {
				datastr = fgets(linestr, 1024, graphFile);
				if (datastr == NULL)
					return -1;

				datastr = strtok(linestr," ");

				datastr = strtok(NULL," ");
				n = atoi(datastr);

				datastr = strtok(NULL," ");
				m = atoi(datastr);

			}
		}
	}

	printf("Graph with %d vertices and %d edges density %f\n", n, m, (double)(2*m)/(n*(n-1)) );


  	for(i=0; i<n; i++) degree[i] = 0;
  	for(i=0;i<n;i++)
		for(j=0;j<n/INT_SIZE+1;j++)
  			bit[i][j] = 0;

  	nedges = 0;
	type = fgetc(graphFile);
	while (type != EOF) {
		/* Edges */
		if (type == 'e') {
			datastr = fgets(linestr, 100, graphFile);


			if (datastr == NULL)
				return -1;

			datastr = strtok(linestr," ");
			i = atol(datastr) - 1;

			datastr = strtok(NULL," ");
			j = atol(datastr) - 1;

			//printf("edge %d %d\n", i , j);



  			if( i != j ){
				nedges++;
				setbit(bit[i], j);
				setbit(bit[j], i);
				degree[i]++;
				degree[j]++;
  			}

		}
		type = fgetc(graphFile);
	}

	int contador = 0;

	for(int i = 0; i < n; i++){
		for(int j = 0; j < n; j++){
			if( test(bit[i],j) ) contador++;
		}
	}

	contador = contador / 2;


	if( contador != m ){
		printf("Error bit adjacency graph representation\n");
		exit(1);
	}

	return 0;
}

void degree_order(){
	int degree[MAX_VERTEX];
	int used[MAX_VERTEX];
	int count;
	int delta = 0;

	/* order vertices */
	for(int i=0;i<Vnbr;i++) {
		degree[i] = 0;
		for(int j=0;j<Vnbr;j++)
  			if ( test(bit[i],j) ) degree[i]++;
			if( degree[i] > delta) delta = degree[i];
  	}

  	for(int i=0;i<Vnbr;i++)
		used[i] = false;

  	count = Vnbr - 1;
  	do{
 		int min_degree;
 		int p;


 		min_degree = delta + 1;
 		p = -1;

 		for(int i=Vnbr-1;i>=0;i--){
   			if((!used[i])&&(degree[i]<min_degree)){
	 			min_degree = degree[i];
	  			p = i;
   			}
 		}

 		pos[count--] = p;
 		used[p] = true;


 		for(int j=0;j<Vnbr;j++)
   			if((!used[j])&&(j!=p)&&(test(bit[p],j)))
	 			degree[j]--;

  	} while(count >= 0);


}


bool isCoKPlex(word * C, int s){

    int W = high(Vnbr);
    word R[W+1];

    for(int i=0; i<=W; i++) R[i] = C[i];
    int iv, jv;

    word S[W+1];

    iv = 0;
    jv = LSB(R, iv, W);

    while(jv >= 0){
        int v = indexbit(iv, jv);

        for(int k = 0; k <= W; k++) S[k] = C[k] & pbit[v][k];

        if(COUNT(S, W)  >= s) return false;

        resetbit(R, v);
        jv = LSB(R, iv, W);
    }

    return true;
}

int menor(int a, int b){ return a < b ? a : b;}

int cocoloring(word * U, int W){
    word R[Vnbr][W+1];
    word S[W+1];
    int l[Vnbr];
    for(int i=0; i<Vnbr; i++) l[i] = 0x7FFFFFFF-s;

    for(int i=0; i<Vnbr; i++){
        for(int j=0; j<=W; j++){
            R[i][j] = 0LL;
        }
    }

    for(int i=0; i<=W; i++){
        S[i] = U[i];
    }

    int iv, jv;
    iv = 0;
    jv = LSB(S, iv, W);

    int max = -1;

    while(jv >= 0){
        int v = indexbit(iv, jv);

        for(int i=0; i<Vnbr; i++){
            setbit(R[i], v);
            if(isCoKPlex(R[i], s)){
                if(i > max) max = i;
                break;
            }else{
                resetbit(R[i], v);
            }
        }
        resetbit(S, v);
        jv = LSB(S, iv, W);
    }
    int iv2, jv2;
    int max_degree[max+1];
    for(int i=0; i<=max; i++) max_degree[i] = 0;

    for(int i = max; i>=0; i--){

        int m = COUNT(R[i], W);

        for(int j=m; j>=0; j--){
            word b[W+1];

            for(int k=0; k<=W; k++) b[k] = R[i][k];
            iv2 = 0;
            jv2 = MSB(b, iv2);
            int cont = 0;

            while(jv2 >= 0){
                int v = indexbit(iv2, jv2);

                if(degree[v] > max_degree[i]) max_degree[i] = degree[v];

                if(degree[v] >= m) cont++;

                resetbit(b, v);
                jv2 = MSB(b, iv2);
            }

            if(cont >= s+m){
                l[i] = m;
                break;
            }
        }
    }

    int bound = 0;
    for(int i=0; i<=max; i++){
        bound += menor(COUNT(R[i], W), menor(2*s-2+2%2, menor(s+l[i], max_degree[i]+s)));
    }

    return bound;
}



int main(int argc, char *argv[]){
	FILE *infile;

  	/* read input */
  	if(argc < 3) {
		printf("Usage: BB [timeout] k infile\n");
		printf("k: k-plex size\n");
		printf("infile: input file path\n");
		exit(1);
  	}

  	timeout = atoi( argv[1] );

  	printf("timeout %d s\n", timeout );

  	if((infile=fopen(argv[3],"r"))==NULL){
		printf("Error in graph file\n %s\n", argv[2]);
		exit(0);
  	}


	if(argv[3])
		s = atoi(argv[2]);

  	/* read graph */
  	readFile(infile, Vnbr, Enbr);

  	/* "start clock" */
  	clk = clock();

  	//ordenaçao smallest-last
  	degree_order();

  	//Reconstrução da matriz adjacência
	for(int i = 0; i < Vnbr; i++){
		for(int k = 0; k < NWORDS(Vnbr); k++){
			pbit[i][k] = 0LL;
		}
	
		for(int j = 0; j < Vnbr; j++){
			if( test( bit[pos[i]], pos[j] ) ){
				setbit( pbit[i], j);
			}
		}
	}

  	word U[ NWORDS(Vnbr) ];
  	for(int k = 0; k < NWORDS(Vnbr); k++){
  		U[k] = 0LL;
  	}
  	for(int i = 0; i < Vnbr; i++){
		setbit(U, i);
  	}


  	subp  = 0LL;
  	printf("start search\n");

	//basicPlexBranching(U, high(Vnbr), 0, nncnt);

	record = cocoloring(U	, high(Vnbr));
  	elapsed = ((double) (clock() - clk)) / CLOCKS_PER_SEC;

	printf("\n");
  	printf("record          =  %10d\n", record );
  	printf("subp            =  %10lld\n", subp );
  	printf("time            =  %10.5f\n", elapsed );
  	//printf("cont1           =  %10lld\n", cont1 );
  	//printf("cont2           =  %10lld\n", cont2 );
  	//printf("cont3           =  %10lld\n", cont3 );
  	//printf("cont4           =  %10lld\n", cont4 );

	return 0;
}
